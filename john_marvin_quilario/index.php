<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>index</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&amp;display=swap">
    <link rel="stylesheet" href="assets/css/styles.css">    
    <title>John Marvin Quilario</title>
    <script src="index.js"></script>
</head>

<body style="font-family: Roboto, sans-serif;height: 100vh;">
    

    <!-- Start: Left Container -->
    <div class="d-flex" style="height: 100%;">
        <!-- Start: Sidebar -->
        <div class="text-center float-start" style="padding: 0;width: 300px;background: linear-gradient(var(--bs-blue) 0%, var(--bs-blue) 0%, white 12%), var(--bs-primary);height: 100%;border-right-width: 1px;border-right-style: solid;">
            <!-- Start: Sidebar Content -->
            <div class="d-xl-flex flex-column align-items-center align-content-center align-self-center justify-content-xl-center align-items-xl-center">
                <!-- Start: Sidebar Logo -->
                <div class="d-xl-flex justify-content-xl-center align-items-xl-start" style="padding: 20px;width: 300px;">
                    <div class="text-center border rounded-circle border-primary shadow d-xl-flex justify-content-xl-center align-items-xl-center" style="width: 150px;border-style: solid;border-color: var(--bs-red);height: 150px;background: #ffffff;"><img src="assets/img/logo.png" style="width: 70%;font-weight: bold;text-align: center;border-style: none;"></div>
                </div><!-- End: Sidebar Logo -->
                <!-- Start: Sidebar Main Container -->
                <div class="d-xl-flex flex-column justify-content-xl-center align-items-xl-start" style="width: 300px;">
                    <!-- Start: Content Container -->
                    <div class="d-xl-flex flex-column justify-content-xl-center align-items-xl-start" style="width: 100%;padding: 0px 20px;">
                        <p class="d-xl-flex justify-content-xl-center" style="width: 100%;font-weight: bold;letter-spacing: 5px;font-family: Roboto, sans-serif;margin: 0;font-size: 20px;">EVERSUN </p>
                        <p class="d-xl-flex justify-content-xl-center" style="width: 100%;letter-spacing: 5px;font-family: Roboto, sans-serif;margin: 0;margin-top: 20px;">CODING ACTIVITY</p>
                    </div><!-- End: Content Container -->
                </div><!-- End: Sidebar Main Container -->
                <!-- Start: Activity Container -->
                <div class="d-xl-flex flex-column justify-content-xl-center align-items-xl-start" style="padding: 0;width: 300px;margin: 20px;margin-top: 0px;">
                    <!-- Start: Label of Content Container -->
                    <div class="d-xl-flex flex-column justify-content-xl-center align-items-xl-start" style="width: 100%;padding: 0px 20px;border-width: 1px;border-style: none;margin-bottom: 20px;">
                        <p class="d-xl-flex justify-content-xl-center" style="width: 100%;font-family: Roboto, sans-serif;margin: 0;text-align: center;letter-spacing: 2px;">Table of Contents</p>
                    </div><!-- End: Label of Content Container -->
                    <!-- Start: Activity Container -->
                    <div class="d-xl-flex flex-column justify-content-xl-center" style="width: 100%;">
                        <!-- Start: Part I Container -->
                        <div style="padding: 20px;width: 100%;height: 100%;font-weight: bold;letter-spacing: 2px;"><span>PART I</span>
                        <!--'loadPage("solution", "double_diamond.php")'-->
                            <div>
                                <ul class="list-unstyled" style="margin-top: 5px;font-family: Roboto, sans-serif;letter-spacing: 0;font-weight: 100;">
                                    <li><a href="double_diamond.php" style="text-decoration: none;color: var(--bs-dark);">Double Diamond Patter</a></li>
                                    <li><a href="x_num_pattern.php" style="text-decoration: none;color: var(--bs-dark);">X Pattern w/ Number Sequence</a></li>
                                    <li><a href="half_diamond_num_seq.php" style="text-decoration: none;color: var(--bs-dark);">Half Diamond Pattern</a></li>
                                    <li><a href="number_seq.php" style="text-decoration: none;color: var(--bs-dark);">Number Sequence</a></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div><!-- End: Part I Container -->
                        <!-- Start: Part II Container -->
                        <div style="padding: 0px 20px;width: 100%;height: 100%;font-weight: bold;letter-spacing: 2px;"><span>PART II</span>
                            <div>
                                <ul class="list-unstyled" style="margin-top: 5px;font-family: Roboto, sans-serif;letter-spacing: 0;font-weight: 100;">
                                    <li><a href="#" style="text-decoration: none;color: var(--bs-dark);">Random Characters Generator</a></li>
                                    <li><a href="#" style="text-decoration: none;color: var(--bs-dark);">Multidimensional Array</a></li>
                                    <li><a href="#" style="text-decoration: none;color: var(--bs-dark);">Array FILO</a></li>
                                    <li><a href="#" style="text-decoration: none;color: var(--bs-dark);">Array FIFO</a></li>
                                    <li><a href="#" style="text-decoration: none;color: var(--bs-dark);">Code Debugging</a></li>
                                </ul>
                            </div>
                        </div><!-- End: Part II Container -->
                    </div><!-- End: Activity Container -->
                </div><!-- End: Activity Container -->
            </div><!-- End: Sidebar Content -->
        </div><!-- End: Sidebar -->
        <!-- Start: Right Content -->
        <div style="padding: 20px;width: 100%;margin-left: 10px;background: linear-gradient(var(--bs-blue), white 12%);">
            <!-- Start: Instruction Container -->
            <div id="container">
                <p>Instructions:</p>
            </div><!-- End: Instruction Container -->
            <!-- Start: Solution Container -->
            <div id="solution">
                <p>Solution</p>
            </div><!-- End: Solution Container -->
        </div><!-- End: Right Content -->
    </div><!-- End: Left Container -->

    <!-- <script type="text/javascript">
        window.onload = function(){
        solution = document.getElementById("solution");
        // solution.innerHTML="<img src='loadingImage.gif'>";
        if(XMLHttpRequest) var x = new XMLHttpRequest();
        else var x = new ActiveXObject("Microsoft.XMLHTTP");
        x.open("GET", "double_diamond", true);
        x.send();
        x.onreadystatechange = function(){
            if(x.readyState == 4){
                if(x.status == 200) solution.innerHTML = x.responseText;
                else solution.innerHTML = "Error loading document";
                }
            }
    } -->
    </script>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
   
</body>

</html>
<?php
    $n = 4;

    $px_1 = $n;
    $px_2 = ($n * 2) + 2;

    $py_1 = $n;
    $py_2 = ($n * 2) + 2;

    // row 1
    for($row = 1; $row <= $n; $row++) {        
        // column
        for ($col=1; $col < ($n * 3) + 3; $col++) { 
            if ($col == $px_1 or $col == $px_2 or $col == $py_1 or $col == $py_2) {
                print "*";
            } else {
                print "&nbsp;&nbsp";
            }
        }
        $px_1--;
        $px_2--;  

        $py_1++;
        $py_2++;
        print "</br>";
    }

    // row 2
    for($row = $n -1; $row >= 1; $row--) {        
        // column
        for ($col=1; $col < ($n * 3)+1; $col++) { 
            if ($col == $px_1+2 or $col == $px_2+2 or $col == $py_1-2 or $col == $py_2-2) {
                print "*";
            } else {
                print "&nbsp;&nbsp";
            }
        }
        $px_1++;
        $px_2++;  

        $py_1--;
        $py_2--;
        print "</br>";
    }

    
?>
function loadPage(id, filename) {
  console.log(`div id: ${id}, filename: ${filename}`)

  let xhttp //prevent loading (retrieving data)
  let element = document.getElementById(id)
  let file = filename

  if (file) {
    xhttp = new XMLHttpRequest()

    xhttp.onreadystatechange = function () {
      if (this.readyState == 4) {
        // 4 indicates that the readystate is done
        if (this.status == 200) {
          element.innerHTML = this.responseText
        }
        if (this.status == 400) {
          element.innerHTML = <h1>Page not found.</h1>
        }
      }
    }

    xhttp.open('GET', `solution/${file}`, true)
    xhttp.send()
    return
  }
}

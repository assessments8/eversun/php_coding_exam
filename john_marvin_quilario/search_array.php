<?php
    $rows = 4;
    $cols = 5;
   
    $lower_case = range(97, 122);
    $upper_case = range(65, 90);

    $to_search = [...range(97, 107), ...range(65, 75)];
    $to_search_even = array_filter($to_search, fn ($i) => !($i & 1));
   
    $alphabets = [...$lower_case, ...$upper_case];

    $count = 0;
    echo "<table border = '1'>";
        for ($row=0; $row < 2; $row++) { 
            echo "<tr>";
                for ($col=0; $col < 11; $col++) { 
                    $to_print = $row == 0 ? chr($lower_case[$count]):$col;
                    echo  "<td>$to_print</td>";
                    $count++;
                }
            echo "</tr>";
        }
    echo "</table>";
    echo "</br>";
        
    $count = 0;
    echo "<table border = '1'>";
        for ($tr=0; $tr < $rows ; $tr++) { 
            $search = shuffle($alphabets);
            
            echo "<tr>";
                for ($td=0; $td < $cols; $td++) {                     
                    $background = in_array($alphabets[$count], $to_search_even) ? '#00FF00': '#0000';
                    $character = chr($alphabets[$count]);
                    
                    echo "<td style='background-color:".$background.";'>$character</td>";
                $count++;  
                }          
            echo "</tr>";
        }
    echo "</table>";
    echo "</br>";
?>
<?php
    $rows = 5;
    $cols = 5;

    $col_sum = [0,0,0,0,""];

    $numbers = range(1, 100);

    echo "<table border = '1'>";
        for ($row=0; $row < $rows; $row++) { 
            echo "<tr>";
                shuffle($numbers);
                $sum = 0;

            for ($col=0; $col < $cols; $col++) { 
                if($row < 4) {                    
                    if($col < 4) {
                        $sum += $numbers[$col];
                        $col_sum[$col] += $numbers[$col];      
                        $to_print = $numbers[$col];
                    } else {
                        $to_print = $sum;
                    }
                    // $to_print = $col == 4 ? $sum : $numbers[$col];              
                } else {
                    $to_print = $col_sum[$col];                    
                }

                

                // if row == 4 print array
                echo "<td>$to_print</td>";

                
            }
            echo $sum;            
            echo "&nbsp;&nbsp";            
            echo "</tr>";
        }
    echo "</table>";
?>
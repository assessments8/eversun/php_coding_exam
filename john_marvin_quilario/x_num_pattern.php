<?php
    $n = 6;
    $px = 1;
    $py = ($n * 2) - 1;
    $num = 6;

    for ($row=1; $row <= $n ; $row++) {         
        for ($col=1; $col <= $n *2 ; $col++) { 
            if($col == $px || $col == $py) { 
                if($row % 2 == 1) {
                    print "*";               
                } else {
                    print $num;
                }
                    
            } else {
                print "&nbsp;&nbsp";
            }
        }
        $px++;
        $py--;
        $num--;
        print "</br>";
    }
    
    $num=2;
    for ($row=$n-1; $row >= 1 ; $row--) {        
        for ($col=1; $col <= $n *2 ; $col++) { 
            if($col == $px || $col == $py) { 
                if($row % 2 == 1) {
                    print "*";               
                } else {
                    print $num;
                }
                    
            } else {
                print "&nbsp;&nbsp";
            }
        }
        $px++;
        $py--;
        $num++; 
        print "</br>";
    }
?>